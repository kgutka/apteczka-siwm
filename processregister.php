<?php
session_start();
include_once "inc/nagl.php";
include_once "utils/dbConnector.php";
include_once "utils/inputUtils.php";
$conn = connectDB();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

    $name = test_input($_POST['name']);
    $surname = test_input($_POST['surname']);
    $age = test_input($_POST['age']);
    $username = test_input($_POST['username']);
    $password_1 = test_input($_POST['password_1']);
    $password_2 = test_input($_POST['password_2']);
    $email = test_input($_POST['email']);
    $errors = array();

    // sprawdzanie czy dane zostały wprowadzone poprawnie
    if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
        array_push($errors, "Imię: tylko litery i białe znaki są dozwolone");
    }
    if (!preg_match("/^[a-zA-Z ]*$/", $surname)) {
        array_push($errors, "Nazwisko: tylko litery i białe znaki są dozwolone");
    }
    $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        array_push($errors, "Błedny format email");
    }
    if (empty($name)) {
        array_push($errors, "Imię jest wymagane");
    }
    if (empty($surname)) {
        array_push($errors, "Nazwisko jest wymagane");
    }
    if (empty($age)) {
        array_push($errors, "Wiek jest wymagany");
    }
    if (!preg_match("/^[0-9]*$/", $age)) {
        array_push($errors, "Wiek może składać się tylko z cyfr");
    }
    if (empty($username)) {
        array_push($errors, "Login jest wymagany");
    }
    if (empty($email)) {
        array_push($errors, "Email jest wymagany");
    }
    if (empty($password_1)) {
        array_push($errors, "Hasło jest wymagane");
    }
    if ($password_1 != $password_2) {
        array_push($errors, "Hasła nie są takie same");
    }

    // dodawanie użytkownika do bazy danych
    if (count($errors) == 0) {
        $password = md5($password_1); // MD5 Message-Digest Algorithm - dodanie bezpieczeństwa, kodowanie

        try {
            $addUser = $conn->query("INSERT INTO users (name, surname, age, username, email, userpass)
            VALUES ('$name', '$surname', $age, '$username', '$email', '$password')"); 
            if ( !$addUser ) {
                $addUser -> free();
                throw new Exception($conn->error);
            }
            $conn->commit();
          }
          catch ( Exception $e ) {
              $conn->rollback(); 
          }
        ?>
        <div class="sucess">
            Dodano użytkownika
        </div>
    <?php } else { ?>
        <div class="error">
            <?php foreach ($errors as $error) : ?>
                <p> <?php echo $error; ?> </p>
            <?php endforeach ?>
        </div>
    <?php } ?>
<?php
} else {
    $conn -> close();
    header('location: login.php');
}
?>