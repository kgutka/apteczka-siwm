<?php
function test_input($data)
{
    // usuwanie bialych znakow z poczatku i konca stringa
    $data = trim($data);

    // usuwa backslashe
    $data = stripslashes($data);

    // zamienia znaki specjalne na HTML entities np. & na &amp
    $data = htmlspecialchars($data);

    return $data;
}
