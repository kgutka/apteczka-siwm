<?php
session_start();
include_once "inc/nagl.php";
include_once "inc/medicamentSearchResults.php";
$conn = connectDB();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  $userRole = $_SESSION['user_role'];
  $userId = $_SESSION['userid'];
  $whereCondition = null;

  if (isset($_POST['submitSort'])) {
    $fromDate = $_POST['showHistoryFrom'];
    $toDate = $_POST['showHistoryTo'];
    $whereCondition = "c.consumption_date >= '$fromDate' AND c.consumption_date <= '$toDate'";
  }
  //jesli nie jesteś adminem to widzisz tylko swoje zażyte leki
  // jesli jestes adminem to widzisz zazyte leki wszystkich uzytkowników
  if ($userRole != "admin") {
    if (!empty($whereCondition)) {
      $whereCondition = $whereCondition . " AND ";
    }
    $whereCondition = $whereCondition . " u.userid = $userId";
  }

  $query = "SELECT c.med_name, c.consumption_amount, c.consumption_date, u.username FROM consumption c JOIN users u ON c.user_id = u.userid";
  if (!empty($whereCondition)) {
    $query = $query . " WHERE " . $whereCondition;
  }
  $query = $query . ";";
  $result = $conn->query($query);
  $rows = $result->fetch_all(MYSQLI_ASSOC);

?>
  <div class="historyRangePicker">
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST" id="fromToForm">
      <label for="showHistoryFrom" class="fadeIn first">Wyświetl historię od: </label>
      <input name="showHistoryFrom" type="datetime-local" class="fadeIn first" <?php if (isset($_POST['showHistoryFrom'])) {
                                                                                  echo 'value="' . $_POST['showHistoryFrom'] . '"';
                                                                                } ?>>
      <label for="showHistoryTo" class="fadeIn first">Wyświetl historię do: </label>
      <input name="showHistoryTo" type="datetime-local" class="fadeIn first" <?php if (isset($_POST['showHistoryTo'])) {
                                                                                echo 'value="' . $_POST['showHistoryTo'] . '"';
                                                                              } ?>>
      <input name="submitSort" id="submitSort" type="submit" class="fadeIn fourth" value="Pokaż">
    </form>
  </div>
  <h3 style="margin-left:20px">Historia spożycia: </h3>
  <table class="table table-bordered table-hover text-center">
    <thead class="thead-light">
      <tr>
        <th scope="col">Nazwa leku</th>
        <th scope="col">Ilość spożytego leku</th>
        <?php if ($userRole == "admin") { ?>
          <th scope="col">Użytkownik</th> <?php } ?>
        <th scope="col">Data spożycia</th>
      </tr>

    </thead>
    <tbody>
      <?php foreach ($rows as $row) { ?>
        <tr>
          <td><?php echo $row['med_name']; ?></td>
          <td><?php echo $row['consumption_amount']; ?></td>
          <?php if ($userRole == "admin") { ?>
            <td><?php echo $row['username']; ?></td> <?php } ?>
          <td><?php echo $row['consumption_date']; ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

  <?php $result->free_result(); ?>
<?php } else {
  header('location: login.php');
}
?>

<?php
$conn->close();
include_once "inc/stopka.php";
?>