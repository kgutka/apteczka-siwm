<?php include_once "./inc/nagl.php";
session_start();

// gdy jesteś zalogowany przekieruj do index.php
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    header('location: index.php');
} else { ?>
    <div class="wrapper fadeInDown">
        <?php
        if (isset($_POST['submit'])) { // process form

            $username = $_POST['login'];
            $password = $_POST['password'];

            $username = stripcslashes($username);
            $password = md5(stripcslashes($password));

            $conn = connectDB();

            // wyszukiwanie w bazie podanego loginu i hasła
            $result = $conn->query("SELECT * FROM users WHERE username = '$username' AND userpass = '$password'");
            $row = $result->fetch_assoc();
            $userid = $row['userid'];
            if (empty($username || $row)) {
                echo "<p>Logowanie się nie powiodło - nieprawidłowy login lub hasło!</p>";
            }
            // jeśli lgoowanie się powiodło - przekaż zmiennej globalnej następujące parametry i przekeiruj do index.php
            if ($row['username'] == $username && $row['userpass'] == $password) {
                session_start();
                $_SESSION['loggedin'] = true;
                $_SESSION['username'] = $username;
                $_SESSION['name'] = $row['name'];
                $_SESSION['userid'] = $row['userid'];
                $_SESSION['user_role'] = $row['role'];
                header("Location: index.php");
                die;
            } else {
                echo "<p>Logowanie się nie powiodło - nieprawidłowy login lub hasło!</p>";
            }
            $conn -> close();
        }
        ?>
        <!-- formularz logowania -->
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <br><br>
                <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST" id="loginform" style="text-align:center;">
                    <div style="display:flex;flex-flow: row nowrap;">
                        <div style="flex: 1"></div>
                        <div style="display: flex; flex-direction: column;align-items: center">
                            <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">
                            <input type="password" id="password" class="fadeIn third" name="password" placeholder="hasło">
                        </div>
                        <div style="display: flex; flex-direction: column; justify-content: flex-end;flex:1;">
                            <img src=./i/icons/eye.png alt="show-password" class="fadeIn third" style="width:30px;height:30px;" onclick="showPassword()"><br>
                        </div>
                    </div> <br>
                    <input name="submit" type="submit" class="fadeIn fourth" value="Zaloguj się">
                </form>
                <div id="formFooter">
                    Nie masz jeszcze konta? <a class="underlineHover" href="./register.php">Zarejestruj się</a><br>
                </div>
            </div>
        </div>
    </div>

    <?php include_once "./inc/stopka.php"; 
} ?>