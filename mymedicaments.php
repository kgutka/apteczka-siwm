<?php
session_start();
include_once "inc/nagl.php";
$conn = connectDB();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  include_once "inc/medicamentSearchResults.php";

  $first_aid_kit_id = $_SESSION['first_aid_kit_id'];

  $query = "SELECT m.first_aid_kit_id, m.id AS medid, m.amount * CAST(l.Opakowanie AS UNSIGNED) - COALESCE(SUM(c.consumption_amount),0) AS currentMedAmount, l.NazwaHandlowa, l.Postac, l.Dawka, l.Opakowanie, m.amount, m.creation_date, m.expiration_date, m.price, m.isDeleted 
            FROM medicaments m 
            JOIN ListaLekow l ON m.medicament_model_id = l.id 
            LEFT JOIN consumption c ON m.id = c.med_id 
            GROUP BY m.id 
            HAVING m.first_aid_kit_id = $first_aid_kit_id
            AND m.isDeleted = false
            AND currentMedAmount > 0;";
  $result = $conn->query($query);

  if ($result) {
    $rows = $result->fetch_all(MYSQLI_ASSOC); ?>

    <h3 style="margin-left:20px">Moja apteczka: </h3>
    <table class="table table-bordered table-hover text-center">
      <thead class="thead-light">
        <tr>
          <th scope="col">Nazwa leku</th>
          <th scope="col">Postać</th>
          <th scope="col">Dawka</th>
          <th scope="col">Opakowanie</th>
          <th scope="col">Ilość opakowań</th>
          <th scope="col">Ilość leku</th>
          <th scope="col">Cena jednego opakowania</th>
          <th scope="col">Data dodania</th>
          <th scope="col">Data ważności</th>
          <th scope="col">Dodatkowe informacje</th>
          <th scope="col">Usuń lek</th>
        </tr>

      </thead>
      <tbody>
        <?php foreach ($rows as $row) { ?>
          <tr>
            <td><?php echo $row['NazwaHandlowa']; ?></td>
            <td><?php echo $row['Postac']; ?></td>
            <td><?php echo $row['Dawka']; ?></td>
            <td><?php echo $row['Opakowanie']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo $row['currentMedAmount']; ?></td>
            <td><?php echo $row['price']; ?></td>
            <td><?php echo $row['creation_date']; ?></td>
            <td><?php echo $row['expiration_date']; ?></td>
            <td>
              <?php if ($row['expiration_date'] < date('Y-m-d')) { ?>
                <div style="color: red">
                  Lek jest przeterminowany!
                </div>
              <?php } ?>
            </td>
            <td>
              <form class="form-group" action="inc/deleteMedicine.php" method="POST" style="display: flex" id="deleteMedicineForm">
                <input type="hidden" name="medToDeleteID" id="medToDeleteID" value="<?= $row['medid']; ?>" />
                <input name="deleteMyMed" id="deleteMyMed" type="submit" value="Usuń" style="background-color:#bf0a0a">
              </form>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <?PHP $result->free_result(); ?>
  <?php } else { ?>
    <div class="error">
      Nie masz żadnych leków
    </div>
<?php }
} else {
  header('location: login.php');
}
?>
<?php
$conn->close();
include_once "inc/stopka.php";
?>