<?php
session_start();
include_once "inc/nagl.php";
?>

<center>
    <div class="fadeInDown first mt-4">
        <h2>Spis treści:</h2><br>
        <a href=#umlDiagram class="nav-link js-scroll-trigger link">
            <h5> &rarr; Diagram UML</h5>
        </a>
        <a href=#useCases class="nav-link js-scroll-trigger link">
            <h5>&rarr; Przypadki użycia</h5>
        </a>
        <a href=#dbSchema class="nav-link js-scroll-trigger link">
            <h5>&rarr; Schemat bazy danych</h5>
        </a>
    </div>
</center>
<br><br><br>
<div class="fadeInDown second">
    <center>
        <h2 id="umlDiagram">Diagram UML Apteczki<h2><br>
                <img src=./i/icons/UMLApteczki.png style="width: 50%; height:50%; margin-left:auto; margin-right:auto;">
    </center>
    <br><br><br>
</div>
<div class="fadeInDown third">

    <center>
        <h2 id="useCases">Przypadki użycia</h2><br><br>

        <pre><b>1. Logowanie</b>
    Cel: Uzyskanie dostępu do systemu
    Warunek początkowy: Użytkownik jest zarejestrowany. 
    Użytkownik nie jest w danej chwili zalogowany.
    Scenariusz:
    - Wyświetlenie okna logowania
    - Wpisanie loginu oraz hasła
    - Weryfikacja poprawności loginu oraz hasła.
    Wynik działania: Użytkownik zyskuje dostęp do programu, 
    wyświetlenie strony głównej i komunikatu powitalnego.
    Błędy: Użytkownik podał błędne dane.
    &rarr; Wyświetlenie komunikatu o niepoprawnym loginie i haśle
    &rarr; Ponowne wyświetlenie okna logowania

    <b>2. Wyświetlenie listy leków</b>
    Cel: Wyświetlenie listy leków w formie tabeli wraz 
    z opisującymi je cechami (np. data ważności, ilość)
    Warunek początkowy: Użytkownik jest zalogowany.
    Scenariusz:
    - Naciśnięcie przycisku "Wyświetl listę leków".
    Wynik działania: Wyświetlenie listy leków w formie tabeli.
    Błędy: Użytkownik posiada przeterminowane leki w swojej apteczce.
    &rarr; Wyświetlenie komunikatu wraz z podaniem liczby leków, 
    które należy usunąć.
    &rarr; W tabeli z lekami pojawia się dodatkowa informacja 
    "Lek jest przeterminowany!"

    <b>3. Dodanie leku</b>
    Cel: Dodanie nowego leku do apteczki użytkownika.
    Warunek początkowy: Użytkownik jest zalogowany.
    Scenariusz:
    - Naciśnięcie przycisku "Dodaj lek"
    - Wpisanie nazwy leku, następnie zatwierdzenie przyciskiem "Szukaj".
    - Przejście do strony, która zawiera formularz z którego 
    można wybrać lek zawierający nazwę leku (wpisanie np. "skinoren" 
    powoduje wyświetlenie się 11 rekordów, gdyż lek ten występuje 
    pod różnymi postaciami). Wybranie odpowiedniego leku, podanie 
    ilości opakowań i podanie daty ważności leku. Następnie zatwierdzenie 
    przyciskiem "Dodaj".
    Wynik działania: Dodanie rekordu do tabeli z lekami.
    Błędy: Użytkownik podaje nieistniejącą nazwę leku.
    &rarr; Wyświetlenie komunikatu "Nie znaleziono takiego leku"

    <b>4. Zażycie leku</b>
    Cel: Uaktualnienie zawartości apteczki w przypadku zażycia leku.
    Warunek początkowy: Użytkownik jest zalogowany.
    Scenariusz:
    - Naciśnięcie przycisku "Zażyj lek"
    - Wybranie z listy posiadanych leków pozycji zażywanej, podanie 
    ilości leku oraz naciśnięcie przycisku "Zażyj lek".
    Błędy: Podanie wartości większej niż posiadana.
    &rarr;  Wyświetlenie komunikatu "Nie masz takiej ilości leku! Podaj inną liczbę" 
    oraz ponowne wyświetlenie formularza.

    <b>5. Szukaj leku w apteczce</b>
    Cel: Znalezienie informacji o wybranym leku w apteczce użytkownika.
    Warunek początkowy: Użytkownik jest zalogowany.
    Scenariusz:
    - Wpisanie nazwy leku w formularz znajdujący się w prawym górnym rogu
    aplikacji, a następnie zatwierdzenie przyciskiem "Szukaj".
    Wynik działania:  Wyświetlenie rekordów zawierających szukane słowo
    Błędy: Użytkownik podaje nazwę leku, którego nie ma w apteczce.
    &rarr; Wyświetlenie komunikatu "Nie znaleziono takiego leku"  </pre>
    </center>
    <br><br><br>
</div>
<div class="fadeInDown fourth">
    <center>
        <h2 id="dbSchema">Schemat bazy danych<h2><br>
    </center>
    <center>
        <img src=./i/icons/dbSchema.png style="width: 50%; height:50%; margin-left:auto; margin-right:auto;">
    </center>
    <br><br><br>
</div>

<?php
include_once "inc/stopka.php";
?>