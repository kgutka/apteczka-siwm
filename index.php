<?php
session_start();
include_once "inc/nagl.php";
?>
<div class="wrapper fadeInDown">
    <div id="formContent" style="background-color: #e3f2fd">
        <br>
        <?php
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
            echo "Witaj, " . $_SESSION['name'] . " (" . $_SESSION['username'] . ")!<br>";
        } else {
            echo "Żeby wyświetlić tę stronę najpierw musisz się zalogować.<br>";
        } ?>
        <br> </div>
</div>
</p>

<?php
include_once "inc/medicamentSearchResults.php";
include_once "inc/stopka.php"; ?>