<?php
session_start();
include_once "./inc/nagl.php";
?>
<center>
  <br> <br>
  <div class="jumbotron fadeInDown first" style="background-color:#4E809B">
    <img src="i/icons/kgutka.jpg" class="rounded" width="18%" height="18%" align="left" style="margin-right: 25px;">
    <h1 class="display-4" style="color:#fff">Karolina Gutka</h1>
    <p class="lead" style="color:#fff; text-align:justify">
      Jestem studentką Inżynierii Biomedycznej na AGH.
      Uwielbiam zdobywać świat, podróżować - z każdego odwiedzonego kraju przywożę mnóstwo zdjęć.
      Interesuje się behawiorystyką zwierząt i tresurą psów (mam 4 psy, więc jest z kim pracować).
      Wolny czas spędzam aktywnie, jeżdżąc na rowerze lub chodząc po górach.</p>
  </div>

  <div class="jumbotron fadeInDown second" style="background-color:#3E5460">
    <img src="i/icons/pkrol.jpg" class="rounded" width="18%" height="18%" align="left" style="margin-right: 25px;">
    <h1 class="display-4" style="color:#fff">Paulina Król</h1>
    <p class="lead" style="color:#fff; text-align:justify">Jestem osobą dobrze zorganizowaną i chętnie uczę się nowych rzeczy.
      Moja zawziętość powoduje, że żadne wyzwanie nie jest mi straszne.
      Swoją przyszłość chciałabym powiązać z Data Science i programowaniem w Pythonie oraz R.
      W wolnych chwilach udaje się na wycieczki rowerowe i spędzam czas z moim psem.
      Przed 30 rokiem życia chciałabym zwiedzić jak najwięcej państw.</p>
  </div>
</center>
<?php include_once "./inc/stopka.php"; ?>