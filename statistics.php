<?php
session_start();
include_once "inc/nagl.php";
include_once "utils/dbConnector.php";
$conn = connectDB();


if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  $userRole = $_SESSION['user_role'];
  $errors = array();

  if ($userRole == "user") {
    array_push($errors, "Statystykę może zobaczyć tylko ADMIN");
  }

  if ($userRole == "admin" || count($errors) == 0) {

    $firstAidKitId = $_SESSION['first_aid_kit_id'];
    $query = "SELECT l.id, l.NazwaHandlowa, l.Opakowanie, SUM(m.amount) AS med_bought, 
                SUM(m.isDeleted * m.amount) AS med_utilized, 
                SUM(m.amount) - SUM(m.isDeleted * m.amount) AS med_current, 
                SUM(m.amount * COALESCE(m.price, 0)) AS total_cash_spent, 
                SUM(m.isDeleted * m.amount * COALESCE(m.price, 0)) AS cash_thrown_away, m.first_aid_kit_id
                FROM medicaments m
                JOIN ListaLekow l 
                ON m.medicament_model_id = l.id
                GROUP BY l.id
                HAVING m.first_aid_kit_id = $firstAidKitId;";

    $rows = $conn->query($query)->fetch_all(MYSQLI_ASSOC);
?>
    <br><br><br>
    <table class="table table-bordered table-hover text-center">
      <thead class="thead-light">
        <tr>
          <th scope="col">Nazwa</th>
          <th scope="col">Opakowanie</th>
          <th scope="col">Ilość zakupionych opakowań</th>
          <th scope="col">Ilość wyrzuconych opakowań</th>
          <th scope="col">Ilość opakowań (obecnie)</th>
          <th scope="col">Wydane pieniądze</th>
          <th scope="col">Niepotrzebnie wydane pieniądze</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($rows as $row) { ?>
          <tr>
            <td><?php echo $row['NazwaHandlowa']; ?></td>
            <td><?php echo $row['Opakowanie']; ?></td>
            <td><?php echo $row['med_bought']; ?></td>
            <td><?php echo $row['med_utilized']; ?></td>
            <td><?php echo $row['med_current']; ?></td>
            <td><?php echo $row['total_cash_spent']; ?></td>
            <td style="color: red"><?php echo $row['cash_thrown_away']; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>

  <?php } else { ?>
    <div class="error">
      <?php foreach ($errors as $error) : ?>
        <p> <?php echo $error; ?> </p>
      <?php endforeach ?>
    </div>
<?php }
} ?>