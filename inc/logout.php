<?php
// kończenie sesji
    session_start();

    //wyczyszczenie tablicy $_SESSION
    $_SESSION = array();

    //pozbycie się plików cookie i usunięcie tymczasowych danych z serwera
    if (ini_get("session.use_cookies")) {

        $params = session_get_cookie_params();
        // setcookie(name of the cookie sent, 
        //           value sent, the time the cookie expires, 
        //           path, domain, secure, httponly)
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    
    session_unset();
    session_destroy();
    header('location: ../index.php');
