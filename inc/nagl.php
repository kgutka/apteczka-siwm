<!DOCTYPE html>
<?php
//wyświetlanie błędów na stronie
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<html>
<?php
// dołączenie tekstów w danym języku
$lang = "pl";
include_once "./lang/$lang/txt.php";
include_once "./utils/inputUtils.php";
include_once "./utils/dbConnector.php";
?>

<head>
	<title><?php echo $txtTytulAplikacji ?></title>
	<meta charset="UTF-8">

	<!-- wykorzystywane przeze mnie arkusze stylów -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="./css/style.css" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script type="text/javascript" src="./js/main.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>

<body>
	<!-- menu -->
	<nav class="navbar navbar-expand-lg navbar navbar-light bg-light">
		<a class="navbar-brand" href="index.php">Strona główna</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">

				<!-- jak nie jesteś zalogowany pokazuj "Logowanie" -->
				<?php if (!isset($_SESSION['loggedin']) || !$_SESSION['loggedin'] == true) { ?>
					<li class="nav-item active">
						<a class="nav-link" href="login.php">Logowanie <span class="sr-only">(current)</span></a>
					</li>
					<!-- rejestracja dostępna tylko dla admina -->
					<?php } else {
					if ($_SESSION['user_role'] == "admin") { ?>
						<li class="nav-item active">
							<a class="nav-link" href="register.php">Rejestracja<span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="userstable.php">Użytkownicy<span class="sr-only">(current)</span></a>
						</li>
				<?php }
				} ?>

				<li class="nav-item">
					<a class="nav-link text-nowrap" href="about.php">O Autorkach</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-nowrap" href="documentation.php">
						Dokumentacja
					</a>
				</li>
			</ul>
		</div>
		<?php if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) { ?>
			<div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="./inc/logout.php">Wyloguj</a>
					</li>
				</ul>
			</div>
		<?php

			// sprawdzenie, czy są w bazie przeterminowane leki
			$conn = connectDB();
			if (empty($_SESSION['first_aid_kit_id'])) {
				$_SESSION['first_aid_kit_id'] = ($conn->query("SELECT id FROM first_aid_kit LIMIT 1;")->fetch_assoc())['id'];
			}
			$first_aid_kit_id = $_SESSION['first_aid_kit_id'];
			$isThereEpiredMed = $conn->query("SELECT COUNT(*) as expired 
												FROM (SELECT m.first_aid_kit_id, m.id AS medid, 
												m.amount * CAST(l.Opakowanie AS UNSIGNED) - COALESCE(SUM(c.consumption_amount),0) AS currentMedAmount, 
												m.amount, m.expiration_date, m.isDeleted 
												FROM medicaments m 
												JOIN ListaLekow l 
												ON m.medicament_model_id = l.id 
												LEFT JOIN consumption c 
												ON m.id = c.med_id 
												GROUP BY m.id 
												HAVING m.first_aid_kit_id = 1 
												AND m.isDeleted = false 
												AND currentMedAmount > 0 
												AND m.expiration_date <= CURDATE()) AS tabela;") or die($conn->error);

			$isThereEpiredMed = $isThereEpiredMed->fetch_assoc();
			$numberOfExpiredMeds = $isThereEpiredMed['expired'];
			if ($numberOfExpiredMeds > 0) {
				echo '<script>alert("Ilość przeterminowanych leków: ' . $numberOfExpiredMeds . '\nSprawdź, które i się ich pozbądź!")</script>';
			}
		} ?>
	</nav>
	<!-- wybór apteczki, np. apteczka domowa/firmowa -->
	<?php if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
		if (empty($_SESSION['first_aid_kit_id'])) {
			$_SESSION['first_aid_kit_id'] = ($conn->query("SELECT id FROM first_aid_kit LIMIT 1;")->fetch_assoc())['id'];
		}
		if (isset($_POST['selectFirstAidKitInput'])) {
			$_SESSION['first_aid_kit_id'] = $_POST['choseFirstAidKitValue'];
		}
		$firstAidKits = $conn->query("SELECT * FROM first_aid_kit;")->fetch_all(MYSQLI_ASSOC); ?>


		<div style="padding: 20px;display: flex;justify-content: space-between;align-items: center;">
			<div style="display: flex">
				<form autocomplete="off" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST" id="choseFirstAidKit">
					<select form="choseFirstAidKit" class="form-control" id="choseFirstAidKitForm" name="choseFirstAidKitValue">
						<option selected="true" disabled="disabled">Zmień apteczkę</option>
						<?php foreach ($firstAidKits as $row) { ?>
							<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?><?php if ($row['id'] == $_SESSION['first_aid_kit_id']) {
																										echo " - aktualna";
																									} ?></option>
						<?php } ?>
					</select>
					<input type="hidden" name="selectFirstAidKitInput" id="selectFirstAidKitInput" value="true" />
					<input name="submitFirstAidKit" id="submitFirstAidKit" type="submit" value="Zmień apteczkę" style="background-color:#bf0a0a;">
				</form>
				
				<div style="width: 30px"></div>
				<button class="btn btn-secondary d-block p-2 m-10" style="width:100px; height:60px" onclick="document.location = 'mymedicaments.php'">Wyświetl listę leków</button>
				<div style="width: 10px"></div>
				<button class="btn btn-secondary d-block p-2" style="width:80px; height:60px" onclick="document.location = 'addMedicament.php'">Dodaj lek</button>
				<div style="width: 10px"></div>
				<button class="btn btn-secondary d-block p-2" style="width:80px; height:60px" onclick="document.location = 'takeMedicine.php'">Zażyj lek</button>
				<div style="width: 10px"></div>
				<button class="btn btn-secondary d-block p-2" style="width:80px; height:60px" onclick="document.location = 'consumptionHistory.php'">Historia spożycia</button>

				<?php if ($_SESSION['user_role'] == "admin") {	?>
					<div style="width: 10px"></div>
					<button class="btn btn-secondary d-block p-2" style="width:90px; height:60px" onclick="document.location = 'statistics.php'">Statystyka</button>
				<?php } ?>

			</div>
			<!-- formularz do wyszukiwania leku w apteczce -->
			<form class="form-group" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST" style="display: flex">
				<input style="width: initial" type="text" name="myMedToFind" class="form-control" id="myMedToFind" placeholder='Podaj nazwę leku'>
				<div style="width: 20px"></div>
				<input name="submitMyMed" id="submitMyMed" type="submit" value="Szukaj" style="margin: 0">
			</form>
		</div>
	<?php
		$conn->close();
	}