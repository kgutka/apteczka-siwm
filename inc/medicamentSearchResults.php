<!-- Szukanie leku w apteczce -->
<?php
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    $searchingErrors = array();

    if (isset($_POST['submitMyMed'])) {

        $myMedToFind = test_input($_POST['myMedToFind']);
        echo '<br><br><h3 style="margin-left:20px">Znalezione leki:</h3>';

        // sprawdzenie czy input jest pusty
        if (empty($myMedToFind)) {
            array_push($searchingErrors, "Nie podałeś nazwy leku");
        }

        $conn = connectDB();
        $query = "SELECT NazwaHandlowa, Postac, Dawka, Opakowanie, amount, creation_date
                FROM medicaments
                JOIN ListaLekow 
                ON medicaments.medicament_model_id = ListaLekow.id
                WHERE NazwaHandlowa LIKE '%$myMedToFind%';";

        // sprawdzenie ilości znaleznionych leków o podanej nazwie
        $query1 = "SELECT COUNT(NazwaHandlowa) 
                    FROM medicaments 
                    JOIN ListaLekow 
                    ON medicaments.medicament_model_id = ListaLekow.id 
                    WHERE NazwaHandlowa 
                    LIKE '%$myMedToFind%';";

        $result = $conn->query($query);
        $numberOfFoundMeds = $conn->query($query1)->fetch_assoc();

        if ($result != false) {
            $rows = $result->fetch_all(MYSQLI_ASSOC);
        } else {
            $rows = [];
            echo 'nie udało się uderzyć do bazy';
            echo $conn->error;
        }


        if ($numberOfFoundMeds['COUNT(NazwaHandlowa)'] == 0) {
            array_push($searchingErrors, "Nie znaleziono takiego leku");
        }

        if (count($searchingErrors) == 0) { ?>

            <table class="table table-bordered text-center">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Nazwa leku</th>
                        <th scope="col">Postać</th>
                        <th scope="col">Dawka</th>
                        <th scope="col">Opakowanie</th>
                        <th scope="col">Ilość opakowań</th>
                        <th scope="col">Data dodania</th>
                    </tr>

                </thead>
                <tbody class="md-8">
                    <?php foreach ($rows as $row) { ?>
                        <tr>
                            <td><?php echo $row['NazwaHandlowa']; ?></td>
                            <td><?php echo $row['Postac']; ?></td>
                            <td><?php echo $row['Dawka']; ?></td>
                            <td><?php echo $row['Opakowanie']; ?></td>
                            <td><?php echo $row['amount']; ?></td>
                            <td><?php echo $row['creation_date']; ?></td>

                            <form action='<?= $_SERVER['PHP_SELF'] ?>' method="post">
                                <input type="hidden" name="id" value="<?php echo $row['userid']; ?>">
                            </form>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <div class="error">
                <?php foreach ($searchingErrors as $error) : ?>
                    <p> <?php echo $error; ?> </p>
                <?php endforeach ?>
            </div>
        <?php }
    }
}

if (!isset($_SESSION)) {
    header('location: index.php');
}
?>