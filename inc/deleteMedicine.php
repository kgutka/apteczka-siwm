<?php
session_start();
include_once "../utils/dbConnector.php";

if (isset($_POST['deleteMyMed'])) {
    $medToDelete = $_POST['medToDeleteID'];
    $conn = connectDB();

    try {
        $query = "UPDATE medicaments SET isDeleted = true WHERE id =" . $medToDelete . ";";
        $result = $conn->query($query);

        if ( !$result ) {
            $result -> free();
            throw new Exception($conn->error);
        }
        $conn->commit();
    }

    catch ( Exception $e ) {
        $conn->rollback(); 
    } ?>

    <div style= "width: 80%; margin: 10px auto; padding: 10px; border: 1px solid #57a942; color: #388824; background: #e1f2de; border-radius: 5px; text-align: center;">
        Pomyślnie usunięto lek
    </div>
<?php }
