<?php
session_start();
include_once "inc/nagl.php";
$conn = connectDB();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        $first_aid_kit_id = $_SESSION['first_aid_kit_id'];

        $myMeds = $conn -> query("SELECT medicaments.id as id, NazwaHandlowa, Postac, Dawka, Opakowanie FROM medicaments JOIN ListaLekow ON medicaments.medicament_model_id = ListaLekow.id WHERE first_aid_kit_id = $first_aid_kit_id;") or die($conn->error);
        $myMeds = $myMeds -> fetch_all(MYSQLI_ASSOC);

    if (isset($_POST['submitMedToTake'])) {

        $errors = array();
        $medToTakeId = test_input($_POST['medToAdd']);
        $medAmount = test_input($_POST['medAmount']);


        $medInfo = $conn -> query("SELECT m.amount * CAST(l.Opakowanie AS UNSIGNED) - COALESCE(SUM(c.consumption_amount),0) AS currentMedAmount FROM medicaments m
        JOIN ListaLekow l ON m.medicament_model_id = l.id
        LEFT JOIN consumption c ON m.id = c.med_id
        GROUP BY m.id
        HAVING m.id = $medToTakeId;") -> fetch_assoc();

        $currentMedAmount = $medInfo['currentMedAmount'];

        if ($medAmount > $currentMedAmount) {
            array_push($errors, "Nie masz takiej ilości leku! Podaj inną liczbę!");
        }
        if (!preg_match("/^[0-9]*$/", $medAmount)) {
            array_push($errors, "Ilość może składać się tylko z cyfr");
        }
        if (empty($medToTakeId)) {
            array_push($errors, "Wybierz lek do zażycia");
        }
        if (empty($medAmount)) {
            array_push($errors, "Podaj ilość zażytego leku");
        }
        if (count($errors) == 0) {
            $userid = $_SESSION['userid'];
            
            $med_name = ($conn -> query("SELECT l.NazwaHandlowa AS medName FROM medicaments m JOIN ListaLekow l ON m.medicament_model_id = l.id WHERE m.id = $medToTakeId;") -> fetch_assoc())['medName'];
            try {
                $queryString = "INSERT INTO consumption (user_id, med_id, consumption_amount, med_name)
                            VALUES ($userid, $medToTakeId, $medAmount, '$med_name');";
                $res = $conn -> query($queryString);
                if ( !$res ) {
                    $res -> free();
                    throw new Exception($conn->error);
                }
                $conn->commit();
            }
            catch ( Exception $e ) {
                $conn->rollback(); 
            }
            ?>
            <div class="sucess">
            Spożyto lek
            </div>
        <?php } else {?>
            <div class="error">
                <?php foreach ($errors as $error) { ?>
                <p> <?php echo $error; ?> </p>
                <?php } ?>
            </div>
    <?php }
    }
    ?>
    <center>
        <form autocomplete="off" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST" id="takeMedicamentForm">
            <select class="form-control" form="takeMedicamentForm" name="medToAdd" style="width:60%">
                <option selected="true" disabled="disabled">Wybierz lek</option>
                <?php foreach ($myMeds as $med) { ?>
                    <option value="<?php echo $med['id']; ?>"><?php echo $med['NazwaHandlowa'] . " " . $med['Postac'] . " " . $med['Dawka'] . " " . $med['Opakowanie']; ?></option>
                <?php } ?>
            </select> <br>
            <input name="medAmount" type="text" placeholder="Ilość leku" style="width:20%">
            <input name="submitMedToTake" type="submit" value="Zażyj lek">
        </form>
    </center>
<?php } else {
    header('location: login.php');
}
include "./inc/stopka.php"; 
?>