<?php
session_start();
include_once "inc/nagl.php";
include_once "utils/dbConnector.php";
$conn = connectDB();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  $userRole = $_SESSION['user_role'];

    if ($userRole == "admin") {
      if (isset($_POST['submit'])) {
        try {
          $sqldelete = $conn->query("DELETE FROM users WHERE userid={$_POST['id']} LIMIT 1;"); ?>
          <div class="sucess">Użytkownik został usunięty</div> <?php
          if ( !$sqldelete ) {
              $sqldelete -> free();
              throw new Exception($conn->error);
          }
          $conn->commit();
        }
        catch ( Exception $e ) {
            $conn->rollback(); 
        }
      }

      $query = "SELECT * FROM users";
      $result = $conn->query($query);
      $rows = $result->fetch_all(MYSQLI_ASSOC);
      ?>
      <br><br><br>
      <h3 style="margin-left:20px">Zarejestrowani użytkownicy: </h3>
      <table class="table table-bordered table-hover text-center">
        <thead class="thead-light">
          <tr>
            <th scope="col">Imię i nazwisko</th>
            <th scope="col">Wiek</th>
            <th scope="col">Email</th>
            <th scope="col">Uprawnienia</th>
            <th scope="col">Usuń użytkownika</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rows as $row) { ?>
            <tr>
              <td><?php echo $row['name'] . " " . $row['surname']; ?></td>
              <td><?php echo $row['age']; ?></td>
              <td><?php echo $row['email']; ?></td>
              <td><?php echo $row['role']; ?></td>
              <td>
                <form action='<?= $_SERVER['PHP_SELF'] ?>' method="post">
                  <input type="hidden" name="id" value="<?php echo $row['userid']; ?>">
                  <button type="submit" name="submit" class="btn btn-secondary" style="background-color:#bf0a0a; width:140px" value="Delete">Usuń</button>
                </form>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    <?PHP $result->free_result();
    }
} else {
  header('location: login.php');
}

$conn->close();
include_once "inc/stopka.php";
?>