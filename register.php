<?php
session_start();
include_once "./inc/nagl.php";

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    $userRole = $_SESSION['user_role'];

    if ($userRole == "admin") { ?>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <br><br>
                <!-- formularz do rejestracji - po wciśnięciu submit -> processregister.php -->
                <form action="processregister.php" method="POST" id="loginform" style="text-align:center;">
                    <input type="text" id="name" class="fadeIn second" name="name" placeholder="Imię">
                    <input type="text" id="surname" class="fadeIn second" name="surname" placeholder="Nazwisko">
                    <input type="text" id="age" class="fadeIn second" name="age" placeholder="Wiek">
                    <input type="text" id="username" class="fadeIn second" name="username" placeholder="Login">
                    <input type="email" id="email" class="fadeIn third" name="email" placeholder="Email">
                    <input type="password" id="password_1" class="fadeIn third" name="password_1" placeholder="Ustaw hasło" style="margin: 5px">
                    <input type="password" id="password_2" class="fadeIn third" name="password_2" placeholder="Potwierdź hasło" style="margin: 5px">
                    <input name="submit" id="submit" type="submit" class="fadeIn fourth" value="Zarejestruj użytkownika">
                </form>
            </div>
        </div>
        <?php include_once "./inc/stopka.php"; ?>
<?php }
} else { ?>
    <div class="error">
    Rejestrować może tylko osoba do tego uprawniona (admin)
    </div>
    
<?php } ?>