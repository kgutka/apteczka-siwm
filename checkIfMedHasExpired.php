<?php

session_start();
include_once "inc/nagl.php";
$conn = connectDB();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  // porównanie z dzisiejszą datą
  $expiredMeds = $conn->query("SELECT NazwaHandlowa, Postac, Dawka, Opakowanie, amount, creation_date, expiration_date 
                                FROM medicaments JOIN ListaLekow ON medicaments.medicament_model_id = ListaLekow.id 
                                WHERE expiration_date < CURDATE();") or die($conn->error);
  $rows = $expiredMeds->fetch_all(MYSQLI_ASSOC);
  ?>

  <h5 class="ml-5">Przeterminowane leki</h5>
  <table class="table table-bordered mt-3 mx-3">
    <thead>
      <tr>
        <th scope="col">Nazwa leku</th>
        <th scope="col">Postać</th>
        <th scope="col">Dawka</th>
        <th scope="col">Opakowanie</th>
        <th scope="col">Ilość opakowań</th>
        <th scope="col">Data dodania</th>
        <th scope="col">Data ważności</th>
      </tr>

    </thead>
    <tbody>
      <?php foreach ($rows as $row) { ?>
        <tr>
          <td><?php echo $row['NazwaHandlowa']; ?></td>
          <td><?php echo $row['Postac']; ?></td>
          <td><?php echo $row['Dawka']; ?></td>
          <td><?php echo $row['Opakowanie']; ?></td>
          <td><?php echo $row['amount']; ?></td>
          <td><?php echo $row['creation_date']; ?></td>
          <td><?php echo $row['expiration_date']; ?></td>
          <td>
            <form action='<?= $_SERVER['PHP_SELF'] ?>' method="post">
              <input type="hidden" name="id" value="<?php echo $row['userid']; ?>">
            </form>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
<?php } else {
  header('location: login.php');
}
?>
<?php
$conn -> close();
include_once "inc/stopka.php"; 
?>