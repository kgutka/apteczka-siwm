<?php
session_start();
include_once "inc/nagl.php";
$conn = connectDB();
?>

<div class="wrapper fadeInDown">
    <?php
    if (isset($_POST['submitMedToAdd'])) {
        $errors = array();

        $medToAddId = test_input($_POST['medToAdd']);
        $medAmount = test_input($_POST['medAmount']);
        $medPrice = test_input($_POST['medPrice']);
        $medExpirationDate = test_input($_POST['medExpirationDate']);

        $comma = ',';
        if (strpos($medPrice, $comma) !== false) {
            array_push($errors, "Liczbę zapisuj przy pomocy kropki");
        }
        if (!preg_match("/^[0-9]*$/", $medAmount)) {
            array_push($errors, "Ilość może składać się tylko z cyfr");
        }
        if (empty($medToAddId)) {
            array_push($errors, "Wybierz lek do dodania");
        }
        if (empty($medExpirationDate)) {
            array_push($errors, "Dodaj datę ważności");
        }
        if (empty($medPrice)) {
            array_push($errors, "Podaj cenę");
        }
        if (empty($medAmount)) {
            array_push($errors, "Podaj ilość opakowań");
        }
        if (count($errors) == 0) {
            $first_aid_kit_id = $_SESSION['first_aid_kit_id'];
            date_default_timezone_set('Europe/Warsaw');
            $creation_date = date("Y-m-d H:i:s");

            try {
                $addMedicament = $conn->query("INSERT INTO medicaments(amount, expiration_date, medicament_model_id, creation_date, first_aid_kit_id, price, isDeleted)
                                                    VALUES ($medAmount, '$medExpirationDate', $medToAddId , '$creation_date', $first_aid_kit_id, $medPrice, false);");

                if (!$addMedicament) {
                    $addMedicament->free();
                    throw new Exception($conn->error);
                }
                $conn->commit();
            } catch (Exception $e) {
                $conn->rollback();
            } ?>
            
            <div class="sucess">
                Pomyślnie dodano lek
            </div>
        <?php } else { ?>
            <div class="error">
                <?php foreach ($errors as $error) : ?>
                    <p> <?php echo $error; ?> </p>
                <?php endforeach ?>
            </div>
    <?php }
    }

    //sprawdzamy czy taki lek w ogóle istnieje
    if (!isset($_POST['medname'])) {
        echo ' ';
    } else {
        $medname = test_input($_POST['medname']);
        $searchMedNames = $conn->query("SELECT id, NazwaHandlowa, Postac, Dawka, Opakowanie FROM ListaLekow WHERE NazwaHandlowa LIKE '%$medname%' ;") or die($conn->error);
        if (!$searchMedNames || $searchMedNames->num_rows == 0) { ?>
            <div class="error">
                Nie znaleziono takiego leku
            </div>
        <?php } else {
        ?>

            <form autocomplete="off" action="<?= $_SERVER['PHP_SELF'] ?>" method="POST" id="addMedicamentForm" style="display:inline-block; text-align:center">

                <select form="addMedicamentForm" class="form-control" id="exampleFormControlSelect2" name="medToAdd" style="width:100%">
                    <option selected="true" disabled="disabled">Wybierz lek</option>
                    <?php foreach ($searchMedNames as $med) { ?>
                        <option value="<?php echo $med['id']; ?>"><?php echo $med['NazwaHandlowa'] . " " . $med['Postac'] . " " . $med['Dawka'] . " " . $med['Opakowanie']; ?></option>
                    <?php } ?>
                </select><br><br>
                <input name="medAmount" type="text" class="fadeIn second" placeholder="Podaj ilość opakowań" style="width:100%"> <br><br>
                <input name="medPrice" type="text" class="fadeIn second" placeholder="Podaj cenę jednego opakowania (cyfrę przedzielaj kropką) " style="width:100%"> <br><br>
                <label for="medExpirationDate">Podaj datę ważności leku</label><br>
                <input name="medExpirationDate" type="date" class="fadeIn second"><br><br><br>
                <input name="submitMedToAdd" id="submitMedToAdd" type="submit" class="fadeIn fourth" value="Dodaj">
            </form>
        <?php } ?>
</div>
<?php
    }
    $conn->close();
    include_once "inc/stopka.php";
?>