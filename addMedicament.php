<?php
session_start();
include_once "inc/nagl.php";
$conn = connectDB();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) { ?>
    <div class="wrapper fadeInDown">
        <br>
        <!-- formularz dodawania leku (nazwa leku do szukania w bazie) -->
        <h5>Dodaj lek: </h5> <br>
        <div id="formContent" class="wrapper">
            <form class="form-group" action="findMedInDB.php" method="POST">
                <input type="text" name="medname" class="form-control" id="medname" placeholder='Podaj nazwę leku (np. Apap) i kliknij "Szukaj"'>
                <input name="submitName" id="submitName" type="submit" class="fadeIn fourth" value="Szukaj">
            </form>

            <?php


            if (!isset($_POST['medname'])) {
                echo ' ';
            } else {
                $medname = test_input($_POST['medname']);
                $searchMedNames = $conn->query("SELECT NazwaHandlowa, Postac, Dawka, Opakowanie 
                                            FROM ListaLekow 
                                            WHERE NazwaHandlowa 
                                            LIKE '%$medname%' ;") or die($conn->error); ?>
                <!-- wyświtlenie selecta do wybrania leku ze znalezionych w bazie  -->
            <?php } ?>
        </div>
    </div>
<?php } else {
    header('location: login.php');
}
$conn->close();
include_once "./inc/stopka.php";
?>